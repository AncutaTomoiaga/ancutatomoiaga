package HomeworkWeek5.hobby;

public class Address {
    private String street;
    private int no;
    private String city;

    public Address(String street, int no, String city) {
        this.street = street;
        this.no = no;
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public int getNo() {
        return no;
    }

    public String getCity() {
        return city;
    }


    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", no=" + no +
                ", city='" + city + '\'' +
                '}';
    }
}
