package HomeworkWeek5.hobby;

import java.util.ArrayList;
import java.util.List;

public class Hobby {
    private String hobby;
    private int frequency;

    List<Address> addresses = new ArrayList<>();
    List<Country> countries = new ArrayList<>();


    public Hobby(String hobby, int frequency) {
        this.hobby = hobby;
        this.frequency = frequency;
    }

    public void addAddress(Address address){
        this.addresses.add(address);
    }

    public void addCountry(Country country){
        this.countries.add(country);
    }

    public void listAddress(){
        for (Address address : addresses){
            System.out.println(address.getStreet() + " " + address.getNo() + " " + address.getCity());
        }
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobby='" + hobby + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                ", countries=" + countries +
                '}';
    }
}
