package HomeworkWeek5.hobby;

public class Country {
    private String country;

    public Country(String country) {
        this.country = country;
    }


    @Override
    public String toString() {
        return "Country{" +
                "country='" + country + '\'' +
                '}';
    }
}
