package HomeworkWeek5;

import HomeworkWeek5.hobby.Address;
import HomeworkWeek5.hobby.Country;
import HomeworkWeek5.hobby.Hobby;
import HomeworkWeek5.person.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
///////////////////////////////////////////////////////////////////////////////////////Create person
        Person person1 = new Hired("Anca", 25);
        Person person2 = new Student("Toader", 20);
        Person person3 = new Unemployed("Darius", 35);
///////////////////////////////////////////////////////////////////////////////////////Create TreeSet
        Set<Person> sortedNamePersons = new TreeSet<>(new MyNameComparator());
        Set<Person> sortedAgePersons = new TreeSet<>(new MyAgeComparator());
//////////////////////////////////////////////////////////////////////////////////////Add person to the TreeSet
        sortedNamePersons.add(person1);
        sortedNamePersons.add(person3);
        sortedNamePersons.add(person2);

        sortedAgePersons.add(person1);
        sortedAgePersons.add(person2);
        sortedAgePersons.add(person3);
///////////////////////////////////////////////////////////////////////////////////////Print persons by name
        System.out.println("Persons by name are:");
        for(Person person:sortedNamePersons){
            System.out.println(person);
        }
///////////////////////////////////////////////////////////////////////////////////////Print persons by age
        System.out.println("\nPersons by age are:");
        for(Person person:sortedAgePersons){
            System.out.println(person);
        }
////////////////////////////////////////////////////////////////////////////////////////////////Create address
        Address address1 = new Address("21 Decembrie 1989", 30, "Brasov");
        Address address2 = new Address("Louis Vuitton", 125, "Paris");
        Address address3 = new Address("Happy", 59, "London");
////////////////////////////////////////////////////////////////////////////////////////////////Create country
        Country country1 = new Country("Romania");
        Country country2 = new Country("France");
        Country country3 = new Country("UK");
///////////////////////////////////////////////////////////////////////////////////////////////Create hobby
        Hobby hobby1 = new Hobby("running", 5);
        Hobby hobby2 = new Hobby("skiing", 4);
        Hobby hobby3 = new Hobby("hiking", 1);
        Hobby hobby4 = new Hobby("swimming", 3);
        Hobby hobby5 = new Hobby("cycling", 2);
/////////////////////////////////////////////////////////////////////////////////////////////Create list of hobbies
        List<Hobby> hobbies1 = new ArrayList<>();
        List<Hobby> hobbies2 = new ArrayList<>();
        List<Hobby> hobbies3 = new ArrayList<>();
        hobbies1.add(hobby1);
        hobbies1.add(hobby2);
        hobbies2.add(hobby3);
        hobbies2.add(hobby4);
        hobbies3.add(hobby5);
        hobbies3.add(hobby1);
/////////////////////////////////////////////////////////////////////////////////////////////Add address to the hobby
        hobby1.addAddress(address3);
        hobby1.addAddress(address1);
        hobby2.addAddress(address1);
        hobby3.addAddress(address2);
        hobby3.addAddress(address3);
        hobby4.addAddress(address2);
        hobby5.addAddress(address3);

//////////////////////////////////////////////////////////////////////////////////////////////Add country to the hobby
        hobby1.addCountry(country3);
        hobby1.addCountry(country1);
        hobby2.addCountry(country1);
        hobby3.addCountry(country2);
        hobby3.addCountry(country3);
        hobby4.addCountry(country2);
        hobby5.addCountry(country3);
        ////////////////////////////////////////////////////////////////////////////////////////////List addresses hobby1
        System.out.println("\nList of addresses for hobby1 are: ");
        hobby1.listAddress();
//////////////////////////////////////////////////////////////////////////////////////////////Create a HashMap
        Map<Person, List<Hobby>> personMap = new HashMap<>();
        personMap.put(person1, hobbies1);
        personMap.put(person2, hobbies3);
        personMap.put(person3, hobbies2);
//////////////////////////////////////////////////////////Print the name of the hobbies and the countries for a person
        System.out.println("\nAnca hobbies are:");
        System.out.println(personMap.get(person1));
        System.out.println("\nToader hobbies are:");
        System.out.println(personMap.get(person2));
        System.out.println("\nDarius hobbies are:");
        System.out.println(personMap.get(person3));

    }
}

