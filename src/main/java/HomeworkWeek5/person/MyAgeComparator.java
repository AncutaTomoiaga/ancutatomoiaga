package HomeworkWeek5.person;

import java.util.Comparator;

public class MyAgeComparator implements Comparator<Person> {
    public int compare(Person o1, Person o2) {
        return Integer.compare(o1.getAge(), o2.getAge());
    }
}
