package HomeworkWeek6.calculator;

import java.util.List;

public class Calculator {
    protected double result;
    private double a, b, res;
    private String expression;

////Create a method that calculate addition and subtraction of numbers, convert the result from mm in the unit specified by the user

    public double calculate (String finalUnit){
        List<String> tokens = List.of(expression.split(" "));
        double finalResult = unitCovertor(Integer.parseInt(tokens.get(0)), tokens.get(1));
        for (int i = 2; i < tokens.size(); i = i + 3){
            a = finalResult;
            b = unitCovertor(Integer.parseInt(tokens.get(i + 1)), tokens.get(i + 2));
            finalResult = operation(tokens.get(i).charAt(0));
        }
        System.out.println("The result in mm is: " + finalResult);
        System.out.println("The result in " + finalUnit + " is: " + unitCovertor(finalResult, "f" + finalUnit));
        return unitCovertor(finalResult, "f" + finalUnit);
    }

//////////////////////////////////////////////////Create a method for addition and subtraction
    public double operation(char op) {
            if (op == '+') {
                res = a + b;
            } else if (op == '-') {
                res = a - b;
            } else {
                System.out.println("\nInvalid input");
            }
            return res;
        }

///////////////////////////////////////////Create a convertor from mm in other unit and in unit specified by the user
    public double unitCovertor (double number, String unit){
        switch (unit) {
            case "mm":
                result = number;
                break;
            case "cm":
                result = number * 10;
                break;
            case "dm":
                result = number * 100;
                break;
            case "m":
                result = number * 1000;
                break;
            case "km":
                result = number * 1000000;
                break;
            case "fmm":
                result = number;
                break;
            case "fcm":
                result = number / 10;
                break;
            case "fdm":
                result = number / 100;
                break;
            case "fm":
                result = number / 1000;
                break;
            case "fkm":
                result = number / 1000000;
                break;
            default:
                result = number;
                System.out.println("Invalid unit.");
        }
        return result;
    }


    public String getExpression() {
        return expression;
    }

    public void setExpression(String expression) {
        this.expression = expression;
    }

    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }
}
