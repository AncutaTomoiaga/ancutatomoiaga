package HomeworkWeek6;


import HomeworkWeek6.calculator.Calculator;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
////////////////////////////////////////////////////////////////////////////////Create a scanner
        Scanner in = new Scanner(System.in);
        System.out.println("Enter the unit of the result:");
//////////////////////////////////////////////////////////////////Create a unit that should be specified by the user
        String finalUnit = in.nextLine();
/////////////////////////////////////////////////////////////////Set an expression
        System.out.print("Expression to compute:\n");
        Calculator calculator = new Calculator();
        calculator.setExpression("1 m + 10 cm - 20 dm + 1 m + 10 mm");
        System.out.println(calculator.getExpression());
/////////////////////////////////////////////////////////////////Print the result in the unit specified by the user
        calculator.calculate(finalUnit);

    }
}
