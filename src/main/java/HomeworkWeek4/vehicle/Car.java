package HomeworkWeek4.vehicle;

import HomeworkWeek4.dacia.Logan;

public abstract class Car implements Vehicle {
    private float availableFuel;
    private int tireSize;
    private String chassisNumber;
    private float fuelConsumedPer100Km;
    private int currentGear;
    private int maxGear;
    private float fuelConsumption;
    private int totalKm;
    private float averageFuelConsumption;

    public Car(int availableFuel, int tireSize, String chassisNumber) {
        this.availableFuel = availableFuel;
        this.tireSize = tireSize;
        this.chassisNumber = chassisNumber;
    }

    @Override
    public void start() {
        fuelConsumption = 0;
        totalKm = 0;
    }

    @Override
    public void stop() {
        averageFuelConsumption = 100 * fuelConsumption / totalKm;
    }

    @Override
    public void drive(int km) {
        if (currentGear > 0 && currentGear <= maxGear) {
            switch (currentGear) {
                case 1:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.07F;
                    break;
                case 2:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.05F;
                    break;
                case 3:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.03F;
                    break;
                case 4:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1;
                    break;
                case 5:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 0.95F;
                    break;
                case 6:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 0.9F;
                    break;
                default:
            }
            switch (tireSize) {
                case 16:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.1F;
                    break;
                case 17:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.2F;
                    break;
                case 18:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1.3F;
                    break;
                default:
                    fuelConsumedPer100Km = fuelConsumedPer100Km * 1;
            }
            fuelConsumption = fuelConsumption + fuelConsumedPer100Km * km / 100;
            availableFuel = availableFuel - fuelConsumption;
            totalKm = totalKm + km;
        }
    }

    public void shiftGear(int gear){
       if (gear > 0 && gear <= maxGear){
           currentGear = gear;
       }else{
           System.out.println("Invalid gear");
       }
    }

    public float getAvailableFuel() {
        return availableFuel;
    }

    public void setAvailableFuel(float availableFuel) {
        this.availableFuel = availableFuel;
    }

    public int getTireSize() {
        return tireSize;
    }

    public void setTireSize(int tireSize) {
        this.tireSize = tireSize;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public float getFuelConsumedPer100Km() {
        return fuelConsumedPer100Km;
    }

    public void setFuelConsumedPer100Km(float fuelConsumedPer100Km) {
        this.fuelConsumedPer100Km = fuelConsumedPer100Km;
    }

    public int getCurrentGear() {
        return currentGear;
    }

    public void setCurrentGear(int currentGear) {
        this.currentGear = currentGear;
    }

    public int getMaxGear() {
        return maxGear;
    }

    public void setMaxGear(int maxGear) {
        this.maxGear = maxGear;
    }

    public float getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(float fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public int getTotalKm() {
        return totalKm;
    }

    public void setTotalKm(int totalKm) {
        this.totalKm = totalKm;
    }

    public float getAverageFuelConsumption() {
        return averageFuelConsumption;
    }

    public void setAverageFuelConsumption(float averageFuelConsumption) {
        this.averageFuelConsumption = averageFuelConsumption;
    }

    @Override
    public String toString() {
        return "Car{" +
                "availableFuel=" + availableFuel +
                ", tireSize=" + tireSize +
                ", chassisNumber='" + chassisNumber + '\'' +
                '}';
    }
}
