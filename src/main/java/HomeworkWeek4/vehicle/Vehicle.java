package HomeworkWeek4.vehicle;

public interface Vehicle {
    void start();
    void drive(int km);
    void stop();
}
