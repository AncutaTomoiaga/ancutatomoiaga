package HomeworkWeek4;

import HomeworkWeek4.dacia.Dacia;
import HomeworkWeek4.dacia.Logan;
import HomeworkWeek4.dacia.Spring;
import HomeworkWeek4.toyota.Avensis;
import HomeworkWeek4.toyota.Corolla;
import HomeworkWeek4.vehicle.Car;

public class Main {
    public static void main(String[] args) {
///////////////////////////////////////////////////////////////////////////////////////Create cars
        Car logan = new Logan(42, 16, "fthjds1346");
        Car spring = new Spring(35, 15, "etwtgd565hjgfj");
        Car avensis = new Avensis(45, 17, "fhf56fhdfhd");
        Car corolla = new Corolla(25, 18, "fhtryry656dfgf");
        System.out.println("Car Logan");
        System.out.println(logan);
////////////////////////////////////////////////////////////////////////////////////////Start Logan
        logan.start();
////////////////////////////////////////////////////////////////////////////////////////Drive Logan
        logan.setMaxGear(Logan.getGears());
        logan.shiftGear(1);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(1);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(2);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(1);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(3);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(2);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(4);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(5);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(5);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(10);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(3);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(2);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
/////////////////////////////////////////////////////////////////////////////////////////////////Stop Logan
        logan.stop();
        System.out.println("Total kms: " + logan.getTotalKm());
        System.out.println("Available fuel: " + logan.getAvailableFuel());
        System.out.println("Average fuel consumption: " + logan.getAverageFuelConsumption());

///////////////////////////////////////////////////////////////////////////////////////////////////Start Logan
        System.out.println("\n\nStart Logan");
        logan.start();
        logan.shiftGear(1);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(1);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(2);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(2);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
        logan.shiftGear(3);
        System.out.println("Gear: " + logan.getCurrentGear());
        logan.setFuelConsumedPer100Km(Logan.getConsumptionPer100Km());
        logan.drive(5);
        System.out.println("Fuel consumed/100km: " + logan.getFuelConsumedPer100Km());
/////////////////////////////////////////////////////////////////////////////////////////////////////////Stop Logan
        logan.stop();
        System.out.println("Total kms: " + logan.getTotalKm());
        System.out.println("Available fuel: " + logan.getAvailableFuel());
        System.out.println("Average fuel consumption: " + logan.getAverageFuelConsumption());

        System.out.println("\n\nCar Spring");
        System.out.println(spring);
////////////////////////////////////////////////////////////////////////////////////////Start Spring
        spring.start();
////////////////////////////////////////////////////////////////////////////////////////Drive Spring
        spring.setMaxGear(Spring.getGears());
        spring.shiftGear(1);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(1);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
        spring.shiftGear(2);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(1);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
        spring.shiftGear(3);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(2);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
        spring.shiftGear(4);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(5);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
        spring.shiftGear(5);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(12);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
        spring.shiftGear(3);
        System.out.println("Gear: " + spring.getCurrentGear());
        spring.setFuelConsumedPer100Km(Spring.getConsumptionPer100Km());
        spring.drive(2);
        System.out.println("Fuel consumed/100km: " + spring.getFuelConsumedPer100Km());
/////////////////////////////////////////////////////////////////////////////////////////////////Stop Spring
        spring.stop();
        System.out.println("Total kms: " + spring.getTotalKm());
        System.out.println("Available fuel: " + spring.getAvailableFuel());
        System.out.println("Average fuel consumption: " + spring.getAverageFuelConsumption());

        System.out.println("\n\nCar Avensis");
        System.out.println(avensis);
////////////////////////////////////////////////////////////////////////////////////////Start Avensis
        avensis.start();
////////////////////////////////////////////////////////////////////////////////////////Drive Avensis
        avensis.setMaxGear(Avensis.getGears());
        avensis.shiftGear(1);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(1);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
        avensis.shiftGear(2);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(1);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
        avensis.shiftGear(3);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(2);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
        avensis.shiftGear(4);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(5);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
        avensis.shiftGear(6);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(20);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
        avensis.shiftGear(3);
        System.out.println("Gear: " + avensis.getCurrentGear());
        avensis.setFuelConsumedPer100Km(Avensis.getConsumptionPer100Km());
        avensis.drive(2);
        System.out.println("Fuel consumed/100km: " + avensis.getFuelConsumedPer100Km());
/////////////////////////////////////////////////////////////////////////////////////////////////Stop Avensis
        avensis.stop();
        System.out.println("Total kms: " + avensis.getTotalKm());
        System.out.println("Available fuel: " + avensis.getAvailableFuel());
        System.out.println("Average fuel consumption: " + avensis.getAverageFuelConsumption());

        System.out.println("\n\nCar Corolla");
        System.out.println(corolla);
////////////////////////////////////////////////////////////////////////////////////////Start Corolla
        corolla.start();
////////////////////////////////////////////////////////////////////////////////////////Drive Corolla
        corolla.setMaxGear(Corolla.getGears());
        corolla.shiftGear(1);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(1);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
        corolla.shiftGear(2);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(1);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
        corolla.shiftGear(3);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(2);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
        corolla.shiftGear(4);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(5);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
        corolla.shiftGear(6);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(15);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
        corolla.shiftGear(3);
        System.out.println("Gear: " + corolla.getCurrentGear());
        corolla.setFuelConsumedPer100Km(Corolla.getConsumptionPer100Km());
        corolla.drive(3);
        System.out.println("Fuel consumed/100km: " + corolla.getFuelConsumedPer100Km());
/////////////////////////////////////////////////////////////////////////////////////////////////Stop Corolla
        corolla.stop();
        System.out.println("Total kms: " + corolla.getTotalKm());
        System.out.println("Available fuel: " + corolla.getAvailableFuel());
        System.out.println("Average fuel consumption: " + corolla.getAverageFuelConsumption());
    }
}
