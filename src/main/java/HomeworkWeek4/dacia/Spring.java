package HomeworkWeek4.dacia;

public class Spring extends Dacia{
    private static int fuelTankSize = 45;
    private static String fuelType = "diesel";
    private static int gears = 5;
    private static float consumptionPer100Km = 7;

    public Spring(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }

    public static int getFuelTankSize() {
        return fuelTankSize;
    }

    public static void setFuelTankSize(int fuelTankSize) {
        Spring.fuelTankSize = fuelTankSize;
    }

    public static String getFuelType() {
        return fuelType;
    }

    public static void setFuelType(String fuelType) {
        Spring.fuelType = fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static void setGears(int gears) {
        Spring.gears = gears;
    }

    public static float getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public static void setConsumptionPer100Km(float consumptionPer100Km) {
        Spring.consumptionPer100Km = consumptionPer100Km;
    }
}
