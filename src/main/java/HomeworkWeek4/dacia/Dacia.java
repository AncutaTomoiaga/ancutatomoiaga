package HomeworkWeek4.dacia;

import HomeworkWeek4.vehicle.Car;

public abstract class Dacia extends Car {
    public Dacia(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }
}
