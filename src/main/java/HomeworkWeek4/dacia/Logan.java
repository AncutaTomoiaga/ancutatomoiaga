package HomeworkWeek4.dacia;

public class Logan extends Dacia{
    private static int fuelTankSize = 48;
    private static String fuelType = "petrol";
    private static int gears = 5;
    private static float consumptionPer100Km = 6;

    public Logan(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }

    public static int getFuelTankSize() {
        return fuelTankSize;
    }

    public static void setFuelTankSize(int fuelTankSize) {
        Logan.fuelTankSize = fuelTankSize;
    }

    public static String getFuelType() {
        return fuelType;
    }

    public static void setFuelType(String fuelType) {
        Logan.fuelType = fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static void setGears(int gears) {
        Logan.gears = gears;
    }

    public static float getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public static void setConsumptionPer100Km(float consumptionPer100Km) {
        Logan.consumptionPer100Km = consumptionPer100Km;
    }
}
