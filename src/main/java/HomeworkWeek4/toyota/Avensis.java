package HomeworkWeek4.toyota;

public class Avensis extends Toyota{
    private static int fuelTankSize = 50;
    private static String fuelType = "diesel";
    private static int gears = 6;
    private static int consumptionPer100Km = 5;

    public Avensis(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }

    public static int getFuelTankSize() {
        return fuelTankSize;
    }

    public static void setFuelTankSize(int fuelTankSize) {
        Avensis.fuelTankSize = fuelTankSize;
    }

    public static String getFuelType() {
        return fuelType;
    }

    public static void setFuelType(String fuelType) {
        Avensis.fuelType = fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static void setGears(int gears) {
        Avensis.gears = gears;
    }

    public static int getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public static void setConsumptionPer100Km(int consumptionPer100Km) {
        Avensis.consumptionPer100Km = consumptionPer100Km;
    }
}
