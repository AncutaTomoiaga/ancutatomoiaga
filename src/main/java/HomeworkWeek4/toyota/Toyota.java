package HomeworkWeek4.toyota;

import HomeworkWeek4.vehicle.Car;

public abstract class Toyota extends Car {
    public Toyota(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }
}
