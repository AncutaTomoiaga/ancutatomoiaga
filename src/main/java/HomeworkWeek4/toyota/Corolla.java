package HomeworkWeek4.toyota;

public class Corolla extends Toyota{
    private static int fuelTankSize = 49;
    private static String fuelType = "petrol";
    private static int gears = 6;
    private static int consumptionPer100Km = 4;

    public Corolla(int availableFuel, int tireSize, String chassisNumber) {
        super(availableFuel, tireSize, chassisNumber);
    }

    public static int getFuelTankSize() {
        return fuelTankSize;
    }

    public static void setFuelTankSize(int fuelTankSize) {
        Corolla.fuelTankSize = fuelTankSize;
    }

    public static String getFuelType() {
        return fuelType;
    }

    public static void setFuelType(String fuelType) {
        Corolla.fuelType = fuelType;
    }

    public static int getGears() {
        return gears;
    }

    public static void setGears(int gears) {
        Corolla.gears = gears;
    }

    public static int getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public static void setConsumptionPer100Km(int consumptionPer100Km) {
        Corolla.consumptionPer100Km = consumptionPer100Km;
    }
}
