package HomeworkWeek6.calculator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class CalculatorTest {

    private Calculator mCalculator;
/////////////////////////////////////////////////Set an expression for class test
    @BeforeEach
    public void setUp(){
        mCalculator = new Calculator();
        mCalculator.setExpression("2 m + 10 cm - 20 dm + 1 m + 10 mm");
        mCalculator.getExpression();
    }

//////////////////////////////Test calculate method, the result is in cm
    @Test
    void finalUnitCm_calculate_success() {
        assertEquals(111, mCalculator.calculate("cm"));
    }

///////////////////////////////Test calculate method, the result is incorrect
    @Test
    void finalUnitCmResultIncorrect_calculate_fail() {
        assertNotEquals(10, mCalculator.calculate("cm"));
    }

////////////////////////////////Test operation method for addition, subtraction, invalid operation,the result is correct
    @Test
    void AddSubtractionInvalidOperationForTwoNumbers_operation_correct() {
        mCalculator.setA(20);
        mCalculator.setB(10);

        assertEquals(30, mCalculator.operation('+'));
        assertEquals(10, mCalculator.operation('-'));
        assertEquals(10, mCalculator.operation('/'));
    }

///////////////////////////////////////////////////////Parameterized test for addition
    public static int[][] dataSetForAdd() {
        return new int[][] { { 1 , 2, 3 }, { 2, 4, 6 }, { 121, 4, 125 } };
    }

    @ParameterizedTest
    @MethodSource("dataSetForAdd")
    void AddTwoNumbers_operation_correct(int[] dataSetForAdd) {
        mCalculator.setA(dataSetForAdd[0]);
        mCalculator.setB(dataSetForAdd[1]);
        int expected = dataSetForAdd[2];
        assertEquals(expected, mCalculator.operation('+'));
    }

///////////////////////////////Test operation method for addition, subtraction, invalid operation, the result is incorrect
    @Test
    void AddSubtractionInvalidOperationForTwoNumbers_operation_incorrect() {
        mCalculator.setA(20);
        mCalculator.setB(10);

        assertNotEquals(5, mCalculator.operation('+'));
        assertNotEquals(5, mCalculator.operation('-'));
        assertNotEquals(5, mCalculator.operation('/'));
    }

////////////////////////Test unitConvertor method, the conversion in mm, other unit is correct
    @Test
    void ConvertUnitInMmOtherUnit_unitCovertor_success() {
        assertTrue(mCalculator.unitCovertor(10, "mm") == 10);
        assertTrue(mCalculator.unitCovertor(10, "cm") == 100);
        assertTrue(mCalculator.unitCovertor(10, "dm") == 1000);
        assertTrue(mCalculator.unitCovertor(10, "m") == 10000);
        assertTrue(mCalculator.unitCovertor(10, "km") == 10000000);
        assertTrue(mCalculator.unitCovertor(10, "fmm") == 10);
        assertTrue(mCalculator.unitCovertor(10, "fcm") == 1);
        assertTrue(mCalculator.unitCovertor(1000, "fdm") == 10);
        assertTrue(mCalculator.unitCovertor(10000, "fm") == 10);
        assertTrue(mCalculator.unitCovertor(100000, "fkm") == 0.1);
        assertTrue(mCalculator.unitCovertor(10, "lm") == 10);
    }

//////////////////////////////////Test unitConvertor method, the conversion in mm, other unit is incorrect
    @Test
    void ConvertUnitInMmOtherUnit_unitConvertor_fail(){
        assertFalse(mCalculator.unitCovertor(10, "mm") == 1);
        assertFalse(mCalculator.unitCovertor(10, "cm") == 25);
        assertFalse(mCalculator.unitCovertor(10, "dm") == 1);
        assertFalse(mCalculator.unitCovertor(10, "m") == 14);
        assertFalse(mCalculator.unitCovertor(10, "km") == 1);
        assertFalse(mCalculator.unitCovertor(10, "fmm") == 15);
        assertFalse(mCalculator.unitCovertor(10, "fcm") == 56);
        assertFalse(mCalculator.unitCovertor(10, "fdm") == 1);
        assertFalse(mCalculator.unitCovertor(10, "fm") == 18);
        assertFalse(mCalculator.unitCovertor(10, "fkm") == 167);
        assertFalse(mCalculator.unitCovertor(10, "lm") == 1);
    }

/////////////////////////////Parameterized test for unitConvertor method, the conversion in mm, other unit is incorrect
    @ParameterizedTest
    @ValueSource(ints = {5, 3, 2})
    void ConvertUnitInMmOtherUnit_unitConvertor_fail(int result){
        assertFalse(mCalculator.unitCovertor(10, "mm") == result);
        assertFalse(mCalculator.unitCovertor(10, "cm") == result);
        assertFalse(mCalculator.unitCovertor(10, "dm") == result);
        assertFalse(mCalculator.unitCovertor(10, "m") == result);
        assertFalse(mCalculator.unitCovertor(10, "km") == result);
        assertFalse(mCalculator.unitCovertor(10, "fmm") == result);
        assertFalse(mCalculator.unitCovertor(10, "fcm") == result);
        assertFalse(mCalculator.unitCovertor(10, "fdm") == result);
        assertFalse(mCalculator.unitCovertor(10, "fm") == result);
        assertFalse(mCalculator.unitCovertor(10, "fkm") == result);
        assertFalse(mCalculator.unitCovertor(10, "lm") == result);
    }
}