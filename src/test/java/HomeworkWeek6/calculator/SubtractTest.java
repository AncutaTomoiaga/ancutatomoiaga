package HomeworkWeek6.calculator;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;


import static org.junit.jupiter.api.Assertions.*;

public class SubtractTest {
////////////////////////Parameterized test for subtract
    public static int[][] dataSetForSubtract() {
        return new int[][] { { 1 , 2, -1 }, { 2, 4, -2 }, { 121, 4, 117 } };
    }


    @ParameterizedTest
    @MethodSource("dataSetForSubtract")
    void SubtractTwoNumbers_operation_correct(int[] dataSetForSubtract) {
        Calculator mCalculator = new Calculator();
        mCalculator.setA(dataSetForSubtract[0]);
        mCalculator.setB(dataSetForSubtract[1]);
        int expected = dataSetForSubtract[2];

        assertEquals(expected, mCalculator.operation('-'));
    }
}
