package HomeworkWeek6.calculator;


import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
@SuiteDisplayName("Our tests")
@SelectClasses({CalculatorTest.class, SubtractTest.class})
public class TestSuite {
}
